import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import fetchRepositories from './utils/fetchRepositories';
import {
  SORT_BY_ISSUES,
  SORT_BY_STARS,
  SORT_BY_NAME
} from './utils/constants';
import Card from './components/Card';

class App extends Component {
  constructor() {
    super();
    this.state = {
      repos: [],
      languageFilter: ''
    }

    this.onChange = this.onChange.bind(this);
  }

  componentWillMount() {
    this.getRepositories();
  }

  render() {
    const renderCards = [];
    this.state.repos.forEach((item)=> {
      renderCards.push(
        <Card {...item} key={item.id}/>
      )
    })

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Starred Repos</h1>
        </header>
        <div>
        <span>Sort by</span>
        <button onClick={() =>this.getRepositories(SORT_BY_NAME)}>Name</button>
          <button onClick={() => this.getRepositories(SORT_BY_STARS)}>Stars</button>
          <button onClick={() => this.getRepositories(SORT_BY_ISSUES)}>Issues</button>
        </div>
        <div>
          <input type='text' placeholder='Filter by Language' onChange={this.onChange} ref="filterInput" value={this.state.languageFilter}/>
          <button
          onClick={() => this.getRepositories(null,this.state.languageFilter)}>
          Search
          </button>
        </div>
        <div className="flex-wrapper">
          {renderCards}
        </div>
      </div>
    );
  }

  getRepositories(sortBy, languageFilter){
  fetchRepositories(sortBy, languageFilter)
  .then(repos => {
    this.setState({ repos });
  })
  .catch (err => console.log(err));
  }

  onChange(e) {
    this.setState({ languageFilter: e.target.value });
  }
}

export default App;
