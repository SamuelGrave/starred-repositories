import React from 'react';
import { string, number } from 'prop-types';

import './style.css';

const propTypes = {
    id: number.isRequired,
    repoName: string.isRequired,
    description: string,
    language: string,
    stargazerCount: number.isRequired,
    openIssues: number.isRequired,
    createdAt: string.isRequired,
    pushedAt: string.isRequired,
    avatarUrl: string.isRequired,
}

function Card({
    id,
    repoName,
    description,
    language,
    stargazerCount,
    openIssues,
    createdAt,
    pushedAt,
    avatarUrl,
}) {
    return(
        <div className="box">
            <div className="header">
                <p className="title">{repoName}</p>
            </div>
                <div className="row">
                    <div className="left">
                    <img src={avatarUrl} className="image" alt={repoName}/>
                    </div>
                        <div className="right">
                        <p>{description}</p>
                        <p>{language}</p>
                        </div>
                    </div>
            <div className="column">
                <p><span>Stars: </span><span>{stargazerCount}</span> <br/>
                    <span>Issues: </span><span>{openIssues}</span></p>
                <p><span>Created At</span> <span>{createdAt}</span><br />
                    <span>Pushed At</span> <span>{pushedAt}</span> </p>
            </div>
        </div>
    )
}

Card.propTypes= propTypes;

export default Card;