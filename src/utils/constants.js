const SORT_BY_STARS = 'stars';
const SORT_BY_ISSUES = 'issues';
const SORT_BY_NAME = 'name';

module.exports = {
    SORT_BY_ISSUES,
    SORT_BY_NAME,
    SORT_BY_STARS,
}
