import {
    SORT_BY_ISSUES,
    SORT_BY_STARS,
    SORT_BY_NAME
} from './constants';

const gitHubUrl = 'https://api.github.com/user/23070/starred?per_page=100&page='

const filterResultByLanguage = (repositories, language) => {
    const filteredResult = [];

    repositories.forEach((repo) => {
        if (repo.language && repo.language.toLowerCase() === language.toLowerCase()) {
            filteredResult.push(repo);
        }
    });

    return filteredResult;
}

const sortResult = (repositories, sort) => {
    switch (sort) {
        case SORT_BY_NAME:
            repositories = repositories.sort((a, b) => {
                return a.name.localeCompare(b.name)
            });
            break;
        case SORT_BY_ISSUES:
            repositories = repositories.sort((a, b) => {
                return b.open_issues - a.open_issues
            });
            break;
        case SORT_BY_STARS:
            repositories = repositories.sort((a, b) => {
                return b.stargazers_count - a.stargazers_count
            });
            break;
        default:
            break;
    }

    return repositories

}

export default async function (sort, languageFilter) {
    let page = 1;
    let result = [];
    do {
        const response = await fetch(`${gitHubUrl}${page}`);
        const body = await response.json();

        page++;
        if (response.status === 200) {
            if (body.length === 0) {
                break;
            }
            result = result.concat(body);
        } else {
            break;
        }
    } while (true);

    if (sort) {
        result = sortResult(result, sort);
    }
    if (languageFilter) {
        result = filterResultByLanguage(result, languageFilter);
    }

    return transform(result);
}

const transform = (repos) => {
    return repos.map((repo) => {
        const createdAt = new Date(repo.created_at);
        const pushedAt = new Date(repo.pushed_at);

        return {
            id: repo.id,
            repoName: repo.name,
            description: repo.description,
            language: repo.language,
            stargazerCount: repo.stargazers_count,
            openIssues: repo.open_issues,
            createdAt: createdAt.toLocaleDateString(),
            pushedAt: pushedAt.toLocaleDateString(),
            avatarUrl: repo.owner.avatar_url,
        };
    })
}